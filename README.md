# ILIModelSearch <!-- omit in toc -->
CLI program and Webapp with API to full text search INTERLIS data models.

- [Components of IliModelSearch](#components-of-ilimodelsearch)
  - [core](#core)
  - [harvester](#harvester)
  - [backend](#backend)
  - [cli](#cli)
- [License](#license)
- [Authors and Contact](#authors-and-contact)

# Components of IliModelSearch
The IliModelSearch Search Application is made of multiple parts: `core`, `harvester`, `backend`, `cli`.

## [core](./core)
This component contains the data model for an IliFile.

## [harvester](./harvester)
The harvester uses `core` and allows to gather all the IliFiles that originate from a root IliFile-Repository. The harvester is more of a library but can be run seperatly if there the `backend` component is setup. However, the `backend` can launch the harvester library directly.

## [backend](./backend)
The backend provides Rest-APIs for uploading and searching IliFiles. It depends on the `core` and `harvester`. The backend is a Spring Boot Webserver that relies on Elastic Search for indexing and processing search queries. 

## [cli](./cli)
The command line interface was developed last and depends on the `core` and `harvester`. The cli allows to store a local database of all IliFiles and provides the ability to run search queries. The cli application is meant as a drop in replacement of the backend webserver.


# License
Copyright (C) 2021 Geometa Lab

The code published in this repository is available under the [ISC license](https://en.wikipedia.org/wiki/ISC_license) (see the file LICENSE.txt). 
There are libraries - like ili2c.jar and others - which have their own license which may be different from this one.


# Authors and Contact
There are many authors involved in this project and it's maintained by many. Contact: @sfkeller.
