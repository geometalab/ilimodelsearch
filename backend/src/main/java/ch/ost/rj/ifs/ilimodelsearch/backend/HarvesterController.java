package ch.ost.rj.ifs.ilimodelsearch.backend;

import ch.ost.rj.ifs.ilimodelsearch.harvester.HarvestRunner;
import ch.ost.rj.ifs.ilimodelsearch.harvester.Main;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executor;

@Slf4j
@RestController
public class HarvesterController {
    private static final String START_URL = "http://models.interlis.ch";
    private final IliFileController controller;
    CompletableFuture<Void> completableFuture;

    @Autowired
    public HarvesterController(IliFileController controller) {
        this.controller = controller;
    }

    @GetMapping("/harvester/run")
    ResponseEntity<String> launchHarvester() {
        if(completableFuture != null && !completableFuture.isDone()) {
            return ResponseEntity.ok("running");
        } else {
            runHarvester();
            return ResponseEntity.ok("started");
        }
    }

    @Async
    protected void runHarvester() {
        completableFuture = CompletableFuture.runAsync(() -> {
            try {
                HarvestRunner harvestRunner = new HarvestRunner(START_URL, controller);
                harvestRunner.run();
            } catch (IOException e) {
                log.error("Harvester threw an uncaught error during execution", e);
            }
        });
    }
}
