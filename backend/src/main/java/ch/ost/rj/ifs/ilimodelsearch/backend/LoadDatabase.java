package ch.ost.rj.ifs.ilimodelsearch.backend;

import ch.ost.rj.ifs.ilimodelsearch.backend.repository.IliFileRepository;
import ch.ost.rj.ifs.ilimodelsearch.core.model.IliFile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.io.IOException;
import java.util.List;

@Configuration
class LoadDatabase {
    private static final Logger log = LoggerFactory.getLogger(LoadDatabase.class);

    @Bean
    CommandLineRunner initDatabase(IliFileRepository repository, ProfileService service) {
        return args -> {
            service.setupPutMapping();
            try {
                service.resetIndex();
            } catch (IOException e) {
                log.error("Couldn't reset ElasticSearch-Index!", e);
            }
            List<IliFile> iliFilesFromDB = repository.findAll();

            iliFilesFromDB.forEach(iliFile1 -> {
                try {
                    service.createEsEntry(iliFile1);
                } catch (IOException e) {
                    log.error("Calling Elastic Search Service raised an Error!", e);
                }
            });
        };
    }

    private void addIliFile(IliFile iliFile, IliFileRepository repository, ProfileService service) {
        IliFile tmpIliFile = repository.save(iliFile);
        log.info("Preloading " + tmpIliFile);
        try {
            service.createEsEntry(tmpIliFile);
        } catch (IOException e) {
            log.error("Calling Elastic Search Service raised an Error!", e);
        }
    }
}