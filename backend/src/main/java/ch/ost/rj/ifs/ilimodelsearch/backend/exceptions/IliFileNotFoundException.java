package ch.ost.rj.ifs.ilimodelsearch.backend.exceptions;

public class IliFileNotFoundException extends RuntimeException {
    public IliFileNotFoundException(String id) {
        super("Could not find IliFile with id: " + id);
    }
}
