package ch.ost.rj.ifs.ilimodelsearch.backend.repository;

import ch.ost.rj.ifs.ilimodelsearch.core.model.IliFile;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IliFileRepository extends JpaRepository<IliFile, String> {

}
