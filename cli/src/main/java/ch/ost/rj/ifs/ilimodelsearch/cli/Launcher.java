package ch.ost.rj.ifs.ilimodelsearch.cli;

import ch.ost.rj.ifs.ilimodelsearch.cli.helper.LoggerSetup;
import ch.ost.rj.ifs.ilimodelsearch.cli.service.ArgumentService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@Slf4j
@SpringBootApplication
@EntityScan(basePackages = {"ch.ost.rj.ifs.ilimodelsearch.core.model", "ch.ost.rj.ifs.ilimodelsearch.harvester.model"})
@EnableJpaRepositories(basePackages = "ch.ost.rj.ifs.ilimodelsearch.cli")
@RequiredArgsConstructor
public class Launcher implements CommandLineRunner {
    private final ArgumentService argumentService;

    public static void main(String[] args) {
        SpringApplication.run(Launcher.class, args);
    }

    @Override
    public void run(String... args) {
        argumentService.parseArgs(args);
        LoggerSetup.setLoggingLevel(argumentService.verbose);
    }
}
