package ch.ost.rj.ifs.ilimodelsearch.cli;

import ch.ost.rj.ifs.ilimodelsearch.cli.repository.IliFileRepository;
import ch.ost.rj.ifs.ilimodelsearch.core.model.IliFile;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Configuration;

import java.net.MalformedURLException;
import java.net.URL;

@Slf4j
@Configuration
public class LoadConfiguration {

//    @Bean
    CommandLineRunner fillWithExampleData(IliFileRepository repository) throws MalformedURLException {
        IliFile iliFile = new IliFile();
        iliFile.setContent("Content");
        iliFile.setName("Name");
        iliFile.setId("id");
        iliFile.setMd5("md5");
        iliFile.setUrl(new URL("http://url.example.com"));
        iliFile.setSchemaLanguage("schema");
        iliFile.setValidEncoding(true);
        iliFile.setVersion("version");

        return args -> {
            log.info("Preloading: " + repository.save(iliFile));
        };
    }
}
