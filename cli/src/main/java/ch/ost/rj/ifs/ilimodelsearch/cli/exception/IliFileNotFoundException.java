package ch.ost.rj.ifs.ilimodelsearch.cli.exception;

public class IliFileNotFoundException extends RuntimeException {
    public IliFileNotFoundException(String id) {
        super("Could not find IliFile with id: " + id);
    }
}
