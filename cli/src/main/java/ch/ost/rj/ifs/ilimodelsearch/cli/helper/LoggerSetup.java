package ch.ost.rj.ifs.ilimodelsearch.cli.helper;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.LoggerContext;
import org.slf4j.LoggerFactory;

public abstract class LoggerSetup {
    public static void setLoggingLevel(int loggingLevel) {
        LoggerContext loggerContext = (LoggerContext) LoggerFactory.getILoggerFactory();
        loggerContext.getLogger("root").setLevel(resolveLogLevel(loggingLevel));
    }

    private static Level resolveLogLevel(int loggingLevel) {
        if (loggingLevel == 0) return Level.toLevel(5000);
        return Level.toLevel(loggingLevel * 10000);
    }
}
