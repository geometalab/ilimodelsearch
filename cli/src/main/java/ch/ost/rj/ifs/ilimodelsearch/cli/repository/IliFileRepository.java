package ch.ost.rj.ifs.ilimodelsearch.cli.repository;

import ch.ost.rj.ifs.ilimodelsearch.core.model.IliFile;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

public interface IliFileRepository extends JpaRepository<IliFile, String> {

}
