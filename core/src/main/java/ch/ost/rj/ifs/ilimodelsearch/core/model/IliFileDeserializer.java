package ch.ost.rj.ifs.ilimodelsearch.core.model;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.Arrays;
import java.util.List;

public abstract class IliFileDeserializer {
    static ObjectMapper mapper = new ObjectMapper();

    /**
     * Converts json to IliFile Data Object
     * @param json Contains the serialized version of an IliFile
     * @return IliFile Data Object
     * @throws JsonProcessingException This exception is thrown when the parser can't interpret the json
     */
    public static IliFile toIliFile(String json) throws JsonProcessingException {
        return mapper.readValue(json, IliFile.class);
    }

    /**
     * Converts json to a list of IliFile Data Objects
     * @param json Contains the serialized version of an array of IliFiles
     * @return List of IliFile Data Objects
     * @throws JsonProcessingException This exception is thrown when the parser can't interpret the json
     */
    public static List<IliFile> toIliFiles(String json) throws JsonProcessingException {
        return Arrays.asList(mapper.readValue(json, IliFile[].class));
    }
}
