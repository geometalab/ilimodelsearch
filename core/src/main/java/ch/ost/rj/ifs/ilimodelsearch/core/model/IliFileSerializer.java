package ch.ost.rj.ifs.ilimodelsearch.core.model;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.List;

public abstract class IliFileSerializer {
    static ObjectMapper mapper = new ObjectMapper();

    /**
     * Converts IliFile Data Object to json
     * @param iliFile IliFile Data Object
     * @return json String
     * @throws JsonProcessingException This exception is thrown when the parser can't interpret the Data Object
     */
    public static String toJson(IliFile iliFile) throws JsonProcessingException {
        return mapper.writerWithDefaultPrettyPrinter().writeValueAsString(iliFile);
    }

    /**
     * Converts list of IliFile Data Objects to json
     * @param iliFiles List of IliFile Data Objects
     * @return json String
     * @throws JsonProcessingException This exception is thrown when the parser can't interpret the Data Objects
     */
    public static String toJson(List<IliFile> iliFiles) throws JsonProcessingException {
        return mapper.writerWithDefaultPrettyPrinter().writeValueAsString(iliFiles);
    }
}
