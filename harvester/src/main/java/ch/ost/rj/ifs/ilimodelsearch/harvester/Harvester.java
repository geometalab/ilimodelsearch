package ch.ost.rj.ifs.ilimodelsearch.harvester;

import ch.ost.rj.ifs.ilimodelsearch.harvester.model.xml.IliModelsXml;
import ch.ost.rj.ifs.ilimodelsearch.harvester.model.xml.IliSiteXml;
import lombok.extern.slf4j.Slf4j;
import org.jdom2.JDOMException;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.TimeoutException;
import java.util.stream.Collectors;

@Slf4j
public class Harvester {
    private final URL startURL;

    private List<String> visitedSources;

    public Harvester(String startURL) throws MalformedURLException {
        this.startURL = new URL(startURL);
    }

    public List<IliModelsXml> getIliModels() {
        List<URL> iliModelURLs = harvestIliModels(startURL);
        List<IliModelsXml> iliModels = new ArrayList<>();

        iliModelURLs.forEach(url -> {
            try {
                iliModels.add(new IliModelsXml(url));
            } catch (JDOMException e) {
                log.error("XML File couldn't be parsed: " + e.getMessage());
            } catch (IOException e) {
                log.error("The following ilimodel.xml url was unreachable: " + url.toString());
            }
        });

        return iliModels;
    }

    private List<URL> harvestIliModels(URL startUrl) {
        LinkedList<URL> remainingHarvestableSources = new LinkedList<>(Collections.singletonList(startUrl));
        List<URL> harvestedIliModelURL = new ArrayList<>();
        visitedSources = new ArrayList<>();

        while (!remainingHarvestableSources.isEmpty()) {
            URL currentSource = remainingHarvestableSources.poll();
            visitedSources.add(currentSource.getHost() + currentSource.getPath());

            try {
                harvestedIliModelURL.add(new URL(currentSource.toString().concat("/ilimodels.xml")));
            } catch (MalformedURLException e) {
                log.error("The URL was malformed: " + e.getMessage());
            }

            remainingHarvestableSources.addAll(getSubsidiarySites(currentSource));
        }

        return harvestedIliModelURL;
    }

    private List<URL> harvestForSources(URL url) {
        List<URL> outputIliSites = new ArrayList<>();
        try {
            URL completeURL = new URL(url.toString().concat("/ilisite.xml"));
            IliSiteXml iliSiteXml = new IliSiteXml(completeURL);
            List<String> subsidiarySites = iliSiteXml.getSubsidiarySites();

            for (String subsidiarySite : subsidiarySites) {
                outputIliSites.add(new URL(subsidiarySite.replaceAll("/$", "")));
            }
        } catch (MalformedURLException e) {
            log.error("The URL was malformed: " + e.getMessage());
        } catch (IOException e) {
            log.error("The following ilisite.xml was unreachable: " + url.toString().concat("/ilisite.xml"));
        } catch (JDOMException e) {
            log.error("XML File couldn't be parsed: " + e.getMessage());
        }
        return outputIliSites;
    }

    private List<URL> getSubsidiarySites(URL currentSource) {
        List<URL> subsidiarySites = harvestForSources(currentSource);
        return subsidiarySites.stream().filter(this::isUnique).collect(Collectors.toList());
    }

    private boolean isUnique(URL url) {
        return !visitedSources.contains(url.getHost().concat(url.getPath()));
    }
}
