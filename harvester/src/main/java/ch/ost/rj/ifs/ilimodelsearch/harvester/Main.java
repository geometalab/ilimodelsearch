package ch.ost.rj.ifs.ilimodelsearch.harvester;

import ch.ost.rj.ifs.ilimodelsearch.core.model.IliFile;
import ch.ost.rj.ifs.ilimodelsearch.harvester.repository.RestRepositoryImpl;
import lombok.extern.slf4j.Slf4j;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

@Slf4j(topic = "Harvester")
public class Main {
    private static final String START_URL = "http://models.interlis.ch";

    public static void main(String[] args) throws IOException {
        log.info("Test");
        RestRepositoryImpl repository = new RestRepositoryImpl();
        HarvestRunner harvestRunner = new HarvestRunner(START_URL, repository);
        harvestRunner.run();
    }


    private static void generateDebugOutput(List<IliFile> iliFiles) throws FileNotFoundException {
        for (IliFile iliFile : iliFiles) {
            String path = "C:\\tmp\\daten\\" + iliFile.getName() + "_v" + iliFile.getVersion() + ".ili";
            PrintWriter fileWriter = new PrintWriter(path);
            fileWriter.print(iliFile.getContent());
            fileWriter.close();
        }
        String path = "C:/tmp/list_of_know_good_ili_urls.txt";
        PrintWriter urlListFileWriter = new PrintWriter(path);

        for (IliFile iliFile : iliFiles) {
            urlListFileWriter.println(iliFile.getUrl());
        }
        urlListFileWriter.close();
    }
}