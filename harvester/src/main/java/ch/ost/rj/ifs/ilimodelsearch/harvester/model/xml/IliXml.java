package ch.ost.rj.ifs.ilimodelsearch.harvester.model.xml;

import org.jdom2.Document;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

public abstract class IliXml {
    protected URL url;
    protected Document document;

    private IliXml(InputStream xmlFile) throws JDOMException, IOException {
        SAXBuilder builder = new SAXBuilder();
        document = builder.build(xmlFile);
    }

    public IliXml(URL url) throws IOException, JDOMException {
        this(url.openStream());
        this.url = url;
    }
}
