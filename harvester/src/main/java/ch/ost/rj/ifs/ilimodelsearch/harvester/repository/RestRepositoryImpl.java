package ch.ost.rj.ifs.ilimodelsearch.harvester.repository;

import ch.ost.rj.ifs.ilimodelsearch.core.model.IliFile;
import ch.ost.rj.ifs.ilimodelsearch.core.model.IliFileDeserializer;
import com.fasterxml.jackson.core.JsonProcessingException;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.client.fluent.Request;
import org.apache.http.client.fluent.Response;
import org.apache.http.entity.ContentType;
import org.apache.http.util.EntityUtils;

import java.io.IOException;

@Slf4j
public class RestRepositoryImpl implements Repository {
    private static final String URL = "http://localhost:8080/ilifiles";

    @Override
    public IliFile findById(String id) {
        try {
            Response response = Request.Get(URL + "/" + id)
                    .useExpectContinue()
                    .version(HttpVersion.HTTP_1_1)
                    .execute();
            HttpResponse httpResponse = response.returnResponse();
            int statusCode = httpResponse.getStatusLine().getStatusCode();
            byte[] bytes = EntityUtils.toByteArray(httpResponse.getEntity());
            String responseString = new String(bytes);

            if(statusCode == 404) {
                log.info("There was no IliFile on the Repository with id: " + id);
                return null;
            } else {
                return IliFileDeserializer.toIliFile(responseString);
            }
        } catch (JsonProcessingException e) {
            log.error("Response from Server couldn't be deserialized!");
            return null;
        } catch (IOException e) {
            log.error("Could connect to backend!");
            return null;
        }
    }

    @Override
    public void put(IliFile iliFile) {
        try {
            String json = iliFile.toJson();
            Response response = Request.Put(URL + "/" + iliFile.getId())
                    .useExpectContinue()
                    .version(HttpVersion.HTTP_1_1)
                    .bodyString(json, ContentType.APPLICATION_JSON)
                    .execute();
            if (response.returnResponse().getStatusLine().getStatusCode() != 200) {
                log.error("Error while PUT request!");
            }
        } catch (JsonProcessingException e) {
            log.error("Couldn't parse IliFile to JSON!", e);
        } catch (IOException e) {
            log.error("Error while connection to Rest API: " + iliFile.getUrl(), e);
        }
    }

    public String resetBackend() {
        String resetCode = "d6316ee30221bd2a9ce7bc50883e1b67";
        String url = "http://localhost:8080/reset/" + resetCode;

        try {
            return Request.Get(url)
                    .useExpectContinue()
                    .execute().returnContent().asString();
        } catch (IOException e) {
            e.printStackTrace();
            return "Something went wrong with the reset calL!";
        }
    }
}
