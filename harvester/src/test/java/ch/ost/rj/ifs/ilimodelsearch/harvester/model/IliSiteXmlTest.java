package ch.ost.rj.ifs.ilimodelsearch.harvester.model;

import ch.ost.rj.ifs.ilimodelsearch.harvester.model.xml.IliSiteXml;
import org.jdom2.JDOMException;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class IliSiteXmlTest {

    @org.junit.jupiter.api.Test
    void getSubsidiarySites() throws IOException, JDOMException {
        //InputData
        File iliSiteXml = new File("src/test/resources/ch/ost/rj/ifs/ilimodelsearch/harvester/model/ilisite.xml");
        IliSiteXml iliSite = new IliSiteXml(iliSiteXml.toURI().toURL());

        //Expected
        List<String> expected = Arrays.asList(
                "http://models.geo.admin.ch",
                "http://models.kkgeo.ch",
                "http://models.geo.llv.li",
                "https://repositorio.proadmintierra.info"
        );

        //Actual
        List<String> actual = iliSite.getSubsidiarySites();

        assertEquals(expected, actual);
    }
}